﻿using System.Data.Entity;
using DocDemo.Data.Initializers;
using DocDemo.Data.Models;

namespace DocDemo.Data
{
    public class Entities : DbContext, IEntities
    {
        public Entities(): base("Entities") {}

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Employee> Employees { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            EntitiesModelCreator.ConfigureConventions(modelBuilder);
            EntitiesModelCreator.OnModelBuilding(modelBuilder);
        }
    }
}
