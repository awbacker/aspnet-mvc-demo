﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using DocDemo.Data.Models;
using WebMatrix.WebData;

namespace DocDemo.Data.Initializers
{
    public class EntitiesInitializer : DropCreateDatabaseIfModelChanges<Entities>
    {
        private const string _adminRole = "Admin";
        private const string _adminUser = "admin";

        protected override void Seed(Entities context)
        {
            Debug.WriteLine("EntitiesInitializer.Seed()");
            base.Seed(context);

            WebSecurity.InitializeDatabaseConnection(
                "Entities",
                "UserProfile",
                "UserId",
                "UserName",
                autoCreateTables: true);

            SeedUsers();
            SeedOther(context);
        }

        private static void SeedOther(Entities context)
        {
            // save the changes that happened before us (e.g. in webSecurity, creating users) just in case
            context.SaveChanges();

            context.Employees.AddRange( new [] {
                new Employee { Name = "Andrew Backer", HourlyRate = 300, CanBillHourly = true, HireDate = new DateTime(2010, 10, 21), MonthlySalary = 9000, Skills = ".NET, Java, Python, MS SQL" },
                new Employee { Name = "Jeff Zhang", HourlyRate = 375, CanBillHourly = true, HireDate = new DateTime(2009, 03, 12), MonthlySalary = 12000, Skills = "PM, Requirements, Java, Oracle" },
                new Employee { Name = "David Liu", HourlyRate = 0, CanBillHourly = false, HireDate = new DateTime(2003, 09, 29), MonthlySalary = 40000, Skills = "CEO things" }
            });
            context.SaveChanges();
        }

        private static void SeedUsers()
        {
            Helpers.AddRole(_adminRole);
            Helpers.AddUser(_adminUser, "Admin User", "jeffzhang@powere2e.com", "+86 186 7846 2856");
            Helpers.AddUserToRole(_adminUser, _adminRole);
        }
    }
}
