﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;

namespace DocDemo.Data.Initializers
{
    public class EntitiesModelCreator
    {
        public static void ConfigureConventions(DbModelBuilder modelBuilder)
        {
            Debug.Write("Configure Conventions");
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //modelBuilder.Entity<X>().HasOptional(p => p.RelField).WithOptionalPrincipal().WillCascadeOnDelete(false);
        }

        public static void OnModelBuilding(DbModelBuilder modelBuilder)
        {
            Debug.WriteLine("OnModelBuilding");
        }
    }
}
