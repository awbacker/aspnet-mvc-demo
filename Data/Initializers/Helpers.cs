﻿using System.Linq;
using System.Web.Security;
using WebMatrix.WebData;

namespace DocDemo.Data.Initializers
{
    public class Helpers
    {
        public static void AddRole(string roleName)
        {
            if (!Roles.RoleExists(roleName)) Roles.CreateRole(roleName);
        }

        public static void AddUser(string userName, string fullName, string email, string phone)
        {
            if (WebSecurity.UserExists(userName)) return;

            WebSecurity.CreateUserAndAccount(
                userName: userName,
                password: userName,
                propertyValues: new { FullName = fullName, Email = email }
                );
        }

        public static void AddUserToRole(string userName, string roleName)
        {
            if (!Roles.GetRolesForUser(userName).Contains(roleName))
            {
                Roles.AddUsersToRoles(new[] { userName }, new[] { roleName });
            }
        }
    }
}