﻿using System;

namespace DocDemo.Data.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        public String Name { get; set; }
        public DateTime HireDate { get; set; }
        public String Skills { get; set; }
        public Decimal MonthlySalary { get; set; }
        public Decimal HourlyRate { get; set; }
        public bool CanBillHourly { get; set; }

        public decimal YearsOfService
        {
            get
            {
                return new Decimal(Math.Round((DateTime.Now - HireDate).TotalDays / 365, 1));
            }
        }  
    }
}
