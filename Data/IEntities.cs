﻿using System;
using System.Data.Entity;
using DocDemo.Data.Models;

namespace DocDemo.Data
{
    public interface IEntities : IDisposable
    {
        DbSet<UserProfile> UserProfiles { get; set; }
        DbSet<Employee> Employees { get; set; }

        int SaveChanges();
    }
}