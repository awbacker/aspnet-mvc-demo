using System;
using System.Web.Mvc;

namespace DocDemo.Web.Util
{
    public class BootstrapContext<TModel>
    {
        public HtmlHelper<TModel> Helper { get; private set; }

        public string DefaultSizeClass { get; private set; }
        public string DefaultLabelClass { get; private set; }
        public string DefaultInputClass { get; private set; }

        public BsFormHelper<TModel> Form { get; private set; }

        public BootstrapContext(HtmlHelper<TModel> helper)
        {
            this.Helper = helper;
            this.Form = new BsFormHelper<TModel>(this);
        }

        public BootstrapContext<TModel> Classes(string sizeClass = "", int? label = null, int? input = null)
        {
            DefaultSizeClass = String.IsNullOrWhiteSpace(sizeClass) ? "col-md" : sizeClass;
            DefaultLabelClass = GetSize(label) ?? GetSize(3);
            DefaultInputClass = GetSize(input) ?? ""; // blank is default 100%
            return this;
        }

        public string GetSizeWithOffset(int size)
        {
            return GetOffset(12 - size) + " " + GetSize(size);
        }

        public string GetSize(int? size)
        {
            if (!size.HasValue) return null;
            if (size > 12) size = 12;
            if (size < 1) size = 1;
            return DefaultSizeClass + "-" + size.Value;
        }

        public string GetOffset(int? size)
        {
            if (!size.HasValue) return null;
            if (size > 12) size = 12;
            if (size < 1) size = 1;
            return DefaultSizeClass + "-offset-" + size.Value;
        }
    }
}