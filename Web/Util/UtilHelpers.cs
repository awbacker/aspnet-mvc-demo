﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DocDemo.Web.Util
{
    public static class UtilHelpers
    {
        public static void AddClass(this IDictionary<string, object> rvd, params string[] values)
        {
            foreach(var v in values)
            {
                AddClass(rvd, v);
            }
        }

        public static void AddClass(this IDictionary<string, object> rvd, string value)
        {
            if (rvd.ContainsKey("class"))
            {
                var nc = rvd["class"] as string;
                if (nc == null) return;
                var parts = nc.Split(" ".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).ToList();
                if (!parts.Contains(value))
                {
                    parts.Add(value);
                }
                rvd["class"] = string.Join(" ", parts);
            }
            else
            {
                rvd.Add("class", value);
            }
        }
    }
}