using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DocDemo.Web.Util
{
    public class BsFormHelper<TModel>
    {
        private BootstrapContext<TModel> Context { get; set; }
        private HtmlHelper<TModel> Helper { get; set; }

        public BsFormHelper(BootstrapContext<TModel> context)
        {
            this.Context = context;
            this.Helper = context.Helper;
        }

        public MvcHtmlString LabelFor<TValue>(Expression<Func<TModel, TValue>> expression, string text = null, int? size = null, object html = null)
        {
            var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(html);
            attrs.AddClass(Context.GetSize(size) ?? Context.DefaultLabelClass);
            attrs.AddClass("control-label");
            return Helper.LabelFor(expression, text, attrs);
        }

        public MvcHtmlString TextBoxFor<TValue>(Expression<Func<TModel, TValue>> expression, int? size = null, object html = null)
        {
            var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(html);
            attrs.AddClass(Context.GetSize(size) ?? Context.DefaultInputClass);
            attrs.AddClass("form-control");
            return Helper.TextBoxFor(expression, attrs);
        }

        public MvcHtmlString PasswordFor<TValue>(Expression<Func<TModel, TValue>> expression, object htmlAttributes = null)
        {
            var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            attrs.AddClass(Context.DefaultInputClass);
            attrs.AddClass("form-control");
            return Helper.PasswordFor(expression, attrs);
        }


    }
}