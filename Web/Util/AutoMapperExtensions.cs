﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;

namespace DocDemo.Web.Util
{

    /// <summary>
    /// Set of extension methods to make working with AutoMapper easier
    /// </summary>
    public static class AutoMapperExtensions
    {
        /// <summary>
        /// CALL THIS LAST! Calls opt.Ignore(...)  for all properties in the destination that HAVE NOT BEEN MAPPED YET from the source.  Normally all 
        /// properies on the *DESTINATION* object *MUST* be present on the source object, or manually ignored.  In many cases this is too many properties
        /// to do manually.  
        /// </summary>
        /// <remarks>
        /// This must be called last!  Unmapped properties are determined by applying all the mapping rules already set, such as 
        /// "IgnorePropertiesStartingWith(string), so all other configuration must be done first.
        /// </remarks>
        /// <param name="mappingExpression">Object the extension method applies to</param>
        public static IMappingExpression<TSrc, TDest> IgnoreAllPropertiesNotInSource<TSrc, TDest>(this IMappingExpression<TSrc, TDest> mappingExpression)
        {
            var sourceType = typeof(TSrc);
            var destinationType = typeof(TDest);
            var existingMap = Mapper.GetAllTypeMaps().First(x => x.SourceType == sourceType && x.DestinationType == destinationType);
            foreach (var property in existingMap.GetUnmappedPropertyNames())
            {
                mappingExpression.ForMember(property, opt => opt.Ignore());
            }
            return mappingExpression;
        }

        /// <summary>
        /// Ignores a property that <b>IS</b> on the destination object, but not on the source object.  
        /// Normally all properties on the destination must be on the source.
        /// </summary>
        /// <param name="mappingExpression">Object the extension method applies to</param>
        /// <param name="destinationMember">The property on the destination type to ignore</param>
        public static IMappingExpression<TSrc, TDest> Ignore<TSrc, TDest>(
            this IMappingExpression<TSrc, TDest> mappingExpression,
            Expression<Func<TDest, object>> destinationMember)
        {
            return mappingExpression.ForMember(destinationMember, opt => opt.Ignore());
        }

        /// <summary>
        /// Ignores a property that <b>IS</b> on the destination object, but not on the source object.  
        /// Normally all properties on the destination must be on the source.
        /// </summary>
        /// <param name="mappingExpression">Object the extension method applies to</param>
        /// <param name="destinationMember">The property on the destination type</param>
        /// <param name="sourceMember">The property on the source type </param>
        public static IMappingExpression<TSrc, TDest> Map<TSrc, TDest, TProp>(
            this IMappingExpression<TSrc, TDest> mappingExpression,
            Expression<Func<TDest, object>> destinationMember,
            Expression<Func<TSrc, TProp>> sourceMember)
        {
            return mappingExpression.ForMember(destinationMember, opt => opt.MapFrom(sourceMember));
        }

        /// <summary>
        /// Uses AutoMapper to map FROM teh list to a List(of TDest)  You MUST define
        /// your mappings before using this!
        /// </summary>
        public static List<TDest> AutoMapTo<TDest>(this IEnumerable source)
        {
            //TODO: why doesn't this work with two types
            return (from object item in source select Mapper.Map<TDest>(item)).ToList();
        }
    }
}
