﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace DocDemo.Web.Util
{
    public static class BsHtmlHelpers
    {
        public static BootstrapContext<TModel> Bs<TModel>(this HtmlHelper<TModel> helper)
        {
            return new BootstrapContext<TModel>(helper);
        }
        public static MvcHtmlString BsLabelFor<T, TValue>(this HtmlHelper<T> helper, Expression<Func<T, TValue>> expression)
        {
            return helper.LabelFor(expression, new { @class = "control-label" });
        }
    }
}