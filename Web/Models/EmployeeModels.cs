﻿using System;
using System.ComponentModel.DataAnnotations;
using DocDemo.Data.Models;
using DocDemo.Web.Util;

namespace DocDemo.Web.Models
{

    public class EmployeeListModel : BasePagedListModel<Employee>
    {
        public string Search { get; set; }
    }

    public class EmployeeEditModel
    {
        [Required]
        public string Name { get; set; }
        public string Skills { get; set; }
        public decimal? HourlySalary { get; set; }
        public bool CanWorkHourly { get; set; }
    }

    public class EmployeeAddModel
    {
        [Required]
        public string Name { get; set; }

        [Required, Display(Name = "Hire Date")]
        public DateTime? HireDate { get; set; }

        [Required]
        public String Skills { get; set; }

        [Required, Display(Name = "Salary (Monthly)")]
        public Decimal MonthlySalary { get; set; }

        [Display(Name = "Rate (Hourly)")]
        public Decimal? HourlyRate { get; set; }

        public bool CanBillHourly { get; set; }
    }


    public class EmpAutMappers : AutoMapper.Profile
    {
        protected override void Configure()
        {
            CreateMap<Employee, EmployeeEditModel>()
                .Map(d => d.Name, s => s.Name)
                .Map(d => d.Skills, s => s.Skills)
                .Map(d => d.CanWorkHourly, s => s.CanBillHourly)
                .Map(d => d.HourlySalary, s => s.HourlyRate)
                .IgnoreAllPropertiesNotInSource();

            CreateMap<EmployeeAddModel, Employee>()
                .Ignore(d => d.EmployeeId); // add model doesn't have an ID
        }
    }
}