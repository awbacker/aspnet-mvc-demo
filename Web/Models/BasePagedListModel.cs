﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace DocDemo.Web.Models
{
    public abstract class BasePagedListModel<T>
    {
        [NonSerialized]
        private IPagedList<T> _items;
        private int _page = 1;

        protected BasePagedListModel()
        {
            this.ItemsPerPage = 10;
        }

        public IPagedList<T> Items
        {
            get { return _items; }
            set { _items = value; }
        }

        public int ItemsCount { get { return HasItems ? Items.TotalItemCount : 0; } }
        public int ItemsPages { get { return HasItems ? Items.PageCount : 0; } }
        public int ItemsPerPage { get; set; }
        public bool HasItems { get { return Items != null && Items.TotalItemCount > 0; } }
        public int Page { get { return _page; } set { _page = Math.Max(0, value); } }

    }
}