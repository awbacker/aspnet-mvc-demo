﻿using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace DocDemo.Web.Controllers
{
    public class BaseController : Controller
    {

        public T GetOr404<T>(DbSet<T> d, params object[] keys) where T : class
        {
            T item = d.Find(keys);
            if (item == null)
            {
                throw new HttpException(404, "The record you requested could not be found");
            }
            return item;
        }

    }
}