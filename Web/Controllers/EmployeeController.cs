﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DocDemo.Data;
using DocDemo.Data.Models;
using DocDemo.Web.Models;
using PagedList;

namespace DocDemo.Web.Controllers
{
    public class EmployeeController : BaseController
    {
        protected IEntities DB { get; set; }

        public EmployeeController(IEntities db)
        {
            this.DB = db;
        }

        public ActionResult Index(EmployeeListModel model)
        {
            IQueryable<Employee> items = DB.Employees.OrderBy(x => x.Name);
            if (!string.IsNullOrWhiteSpace(model.Search))
            {
                model.Search = model.Search.Trim();
                items = items.Where(x => x.Name.Contains(model.Search) || x.Skills.Contains(model.Search));
            }
            // this line could be improved by making the 'topagedlist' call either part of model
            model.Items = items.ToPagedList(model.Page, model.ItemsPerPage);
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = GetOr404(DB.Employees, id);
            return View(Mapper.Map<EmployeeEditModel>(model));
        }

        [HttpPost]
        public ActionResult Edit(int id, EmployeeEditModel model)
        {
            if (ModelState.IsValid == false) return View();
            var employee = GetOr404(DB.Employees, id);

            employee.HourlyRate = model.HourlySalary ?? 0;
            employee.CanBillHourly = model.CanWorkHourly;
            employee.Name = model.Name;
            employee.Skills = model.Skills;

            DB.SaveChanges();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new EmployeeAddModel());
        }

        [HttpPost]
        public ActionResult Create(EmployeeAddModel model)
        {
            if (!ModelState.IsValid) return View();
            var emp = Mapper.Map<Employee>(model);
            DB.Employees.Add(emp);
            DB.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id, bool confirm = false)
        {
            var employee = GetOr404(DB.Employees, id);
            if (confirm)
            {
                DB.Employees.Remove(employee);
                DB.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                return View(employee);
            }
        }
    }
}