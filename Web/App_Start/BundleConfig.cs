﻿using System.Web.Optimization;

namespace DocDemo.Web
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Content/moment-js/moment.js",
                "~/Content/bootstrap/js/bootstrap.js",
                "~/Scripts/jquery.unobtrusive*",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.validate.unobtrusive.bootstrap.js",
                "~/Content/datepicker/bootstrap-datetimepicker.js", 
                "~/Content/Site.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap/css/bootstrap.css",
                "~/Content/datepicker/bootstrap-datetimepicker.css",
                "~/Content/site.css"));
        }
    }
}