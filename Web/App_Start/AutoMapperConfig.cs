﻿using System;
using System.Linq;
using System.Reflection;

namespace DocDemo.Web
{
    /// <summary>
    /// Automatically loads all automapper profiles that are found in the application
    /// </summary>
    public static class AutoMapperConfig
    {
        private static NLog.Logger _log = NLog.LogManager.GetCurrentClassLogger();

        public static void Configure()
        {
            var profileType = typeof(AutoMapper.Profile);
            var profiles = Assembly.GetExecutingAssembly().GetTypes()
                        .Where(t => t.IsClass)
                        .Where(profileType.IsAssignableFrom);

            foreach (var p in profiles)
            {
                _log.Debug("AUTOMAPPER CONFIG: " + p.FullName);
                try
                {
                    var profile = Activator.CreateInstance(p) as AutoMapper.Profile;
                    if (profile != null)
                    {
                        AutoMapper.Mapper.AddProfile(profile);
                        AutoMapper.Mapper.AssertConfigurationIsValid(profile.ProfileName);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error("Error running Automapper profile: " + ex.Message, ex);
                    throw;
                }
            }
        }
    }
}