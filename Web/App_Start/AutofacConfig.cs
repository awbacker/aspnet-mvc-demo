﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DocDemo.Data;

namespace DocDemo.Web
{
    public class AutofacConfig
    {
        public static void Configure()
        {
            // https://code.google.com/p/autofac/wiki/MvcIntegration

            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterModule(new AutofacWebTypesModule());

            builder.RegisterType<Entities>().As<IEntities>().InstancePerHttpRequest();

            builder.RegisterSource(new ViewRegistrationSource());
            builder.RegisterFilterProvider();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(builder.Build()));
        }
    }
}