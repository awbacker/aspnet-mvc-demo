﻿using System.Data.Entity;
using DocDemo.Data;
using DocDemo.Data.Initializers;

namespace DocDemo.Web
{
    /// <summary>
    /// Do the database config here instead of in the web.config/app.config.  It is so much clearer what we are doing
    /// when its in code instead of a mess of XML
    /// </summary>
    public class DatabaseConfig
    {
        public static void Config()
        {
            Database.SetInitializer<Entities>(new EntitiesInitializer());

            using (var context = new Entities())
            {
                //// create the database FILE if it does not exist yet.  The initialzier code run after 
                //// will not create the file
                //if (!context.Database.Exists())
                //{
                //    Debug.WriteLine("Creating database...");
                //    // Create the SimpleMembership database without Entity Framework migration schema
                //    ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                //}
                context.Database.Initialize(false);
            }
        }
    }
}