﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DocDemo.Web.Util.Mvc;

namespace DocDemo.Web.App_Start
{
    public class ModelBinderConfig
    {
        public static void Config()
        {
            // add a custom model binder that looks at the "DisplayFormat" attribute so we can properly bind
            // YYYY-MM-DD dates and so on.  .Net understand "2014-02-01" so no need yet, I think, but will come
            // in handy in the future
            // ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());
        }
    }
}