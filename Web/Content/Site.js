﻿// submits the #SearchForm for a page, setting the hidden value of the Page input to the next page
// before doing so.  This lets us avoid passing the whole model
function pager_submitForm(action, pageNum) {
    var form = $("#search-form");
    if (form.length == 0) { alert('<form id="search-form"> not found. Can not change page.'); return false; }

    if (action != '') {
        form.attr("action", action);
    }

    var pageInput = form.find("input[id=Page]");
    if (pageInput.length == 0) {
        alert("<input id='Page'> not found in #search-form.  Can not change page.");
        return false;
    }

    pageInput.val(pageNum);
    form.submit();
    return true;
}

function pager_goToPage(action) {
    var toPage = $("#GoToPage");
    if (toPage.length == 0) { alert("<input id='GoToPage'> not found"); }
    var val = toPage.val();
    if (isNaN(+val)) {
        toPage.val('1');
        return false;
    }
    return pager_submitForm(action, toPage.val());
}